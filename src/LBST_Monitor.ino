/*
LilyGo TTGO LoRa V2.1 868 MHz esp32 von aliexpress oder banggood
  https://de.aliexpress.com/item/32915894264.html?spm=a2g0x.search0104.3.1.6adb7756miY1Hk&ws_ab_test=searchweb0_0%2Csearchweb201602_2_10320_10065_10068_10547_319_317_10548_10696_10924

mit
- RTC clock
  https://github.com/jarzebski/Arduino-DS3231/blob/master/DS3231_dateformat/DS3231_dateformat.ino
  RTC mit 5V angeschlossen (Ladung für integrierten Akku)

- (LoRa nicht implementiert weil kein LoRa Netz in Thalkirchen) und sd-Karte am spi-Bus,
  https://github.com/jonashoechst/ttgo-lora-sd/blob/master/src/main.cpp#L56

- oled display und rtc DS3231 für Uhrzeit und Außentemperatur am I2C-Bus 
  https://github.com/ThingPulse/esp8266-oled-ssd1306/blob/master/examples/SSD1306SimpleDemo/SSD1306SimpleDemo.ino

- sowie HX711 Wägesensor für die 2 Bosche H20A Wägezellen 0-200kg (2x100kg ||) nach Bogde mit dem Lemio patch:
  https://github.com/bogde/HX711/issues/75
  https://github.com/lemio/HX711

  HX711 Platine mit 5V angeschlossen

  aus Kalibierung:
  tara = 193417
  scale = 21.18


pin-Belegung des boards s. unten (SD_* und LORA_*)
  GPI12:  Data HX711 Beute 1@LBST; Data = input
          einziger freier pin (eigentlich SD2/DAT2 bei SD-Karte, dort aber nicht benutzt) an dem es funktioniert
          alle anderen pins wie 26, 19, 23, 34, 35, 36,39 gehen NICHT!?
  GPIO25: Clock HX711 Beute 1@LBST gemeinsam mit LED_BUILTIN; Clock = output
  GPIO21: sda für I2C (RTC)
  GPIO22: scl für I2C (RTC)

20.11.2019 Ditmar Unger, ditmar@d-unger.de
*/

// libs
#include <Wire.h>  
#include <HX711.h>                                  // HX711.[cpp,h] von Lemio
#include "SSD1306.h"                                // display
#include "uRTCLib.h"                                // RTC
#include "SPI.h"                                    // SPI-Bus
#include "SD.h"                                     // SD-Karte

const String id = "1";                              // Beute 1 am LBST

// für Differenzmessung gegen letzten Wert um Mitternacht in den Stockwaagen:
String strgewicht;
long gewicht_mitternacht = 0;                       // erster Wert Mitternacht als Referenz für Tagessunmme
long letztesgewicht = 0;                            // Gewicht Beute letzter Durchlauf
long aktuellesgewicht = 0;                          // aktuelles Gewicht
long gewichtszunahmeprotag = 0;                     // Gewichtszunahme seit Mitternacht
long gewichtsdifferenz = 0;                         // Differenz zu letztem Durchlauf
// wenn Gewichtsdifferenz >200g (ein Dadant Honigrähmchen wiegt 209g) pro 5 Min. war's der Imker und nicht die Bienen -> Korrektur
long gewichtskorrektur = 0;
unsigned int datum=0, letztesdatum=0;               // Feststellung Tageswechsel oder reboot

struct Gewicht {
  const byte data;
  const byte clock;
  long tara;
  float scale;
  long weight;
  long lastweight;
  const byte id;
};

Gewicht gewicht[1] = {{12, 25, 193417, 21.18, 9999, 0, 1}}; // Beute LBST
HX711 scale[1];

unsigned int update_interval = 300000;                      // alle 5 Minuten in Millisekunden Werte auf SD Karte
unsigned long update_interval_tmp = 0;
bool verboseSerial=true;

// I2C bus für display + rtc ds3231
#define I2Csda 21                  
#define I2Cscl 22                  

// OLED display
#define DisplayPin 16

// SD Card
#define SD_MISO 2
#define SD_CS 13
#define SD_CLK 14
#define SD_MOSI 15

#define SDCARD_FILE "/LBST_Data.csv"

// The sd card can also use a virtual SPI bus
//SPIClass sd_spi(VSPI);
SPIClass sd_spi(HSPI);

/*
// LoRa pins bei TTGO LiLyGo V2.1, T3_V1.6
#define LORA_SCK 5
#define LORA_CS 18
#define LORA_IRQ 23
#define LORA_RST 23
#define LORA_MISO 19
#define LORA_MOSI 27

#define LORA_FREQ 868.0                         // 868 MHz für LoRa

RHSoftwareSPI sx1278_spi;
RH_RF95 rf95(LORA_CS, LORA_IRQ, sx1278_spi);
*/

SSD1306 display(0x3c, I2Csda, I2Cscl);          // display
uRTCLib rtc(0x68, URTCLIB_MODEL_DS3231);        // rtc

char displaybuffer[50];                         // String für display Ausgabe
char sdcardbuffer[50];                          // String zum Anhängen in FILE auf SD-Karte


//*************************************************************************
void initSD() {
/*
  pinMode(LORA_RST, OUTPUT);
  digitalWrite(LORA_RST, LOW);
  delay(100);
  digitalWrite(LORA_RST, HIGH);

  // the pins for the virtual SPI explicitly to the internal connection
  sx1278_spi.setPins(LORA_MISO, LORA_MOSI, LORA_SCK);
  rf95.init();
*/
  sd_spi.begin(SD_CLK, SD_MISO, SD_MOSI, SD_CS);
  delay(100);
  display.setFont(ArialMT_Plain_16);          // 10, 16 oder 24
  if (!SD.begin(SD_CS, sd_spi)) {
     Serial.println("SD Card: mounting failed.");
     sprintf(displaybuffer, "keine SD Karte\neingelegt?");
  } else {
     Serial.println("SD Card: mounted.");
     if (!SD.exists(SDCARD_FILE)) {
       Serial.println("Datei LBST_Data.csv existiert nicht. Lege Datei neu an.");
       File sdwrite = SD.open(SDCARD_FILE, FILE_WRITE);
       sprintf(sdcardbuffer, "Datum Zeit; Temperatur [°C]; Gewicht [g]; Gewichtskorrektur [g]; Gewichtszunahme pro Tag [g]");
       sdwrite.println(String(sdcardbuffer));
       sdwrite.close();
     }
     sprintf(displaybuffer, "SD Card\nmounted,");
     displayout();
     delay(1000);
  }
  displayout();
}
//*************************************************************************


//*************************************************************************
void initDisplay() {
  pinMode(DisplayPin,OUTPUT);
  digitalWrite(DisplayPin, LOW);                // set GPIO16 low to reset OLED
  delay(50); 
  digitalWrite(DisplayPin, HIGH);               // while OLED is running, must set GPIO16 in high

  display.init();
  display.flipScreenVertically();  
  display.setFont(ArialMT_Plain_16);
}
//*************************************************************************


//*************************************************************************
void displayout() {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 0, displaybuffer);
  display.display();
}
//*************************************************************************


//*************************************************************************
void sdout() {
  File sdwrite = SD.open(SDCARD_FILE, FILE_APPEND);
  display.setFont(ArialMT_Plain_10);          // 10, 16 oder 24
      if (!sdwrite) {
          sprintf(displaybuffer, "... keine SD-Karte\n eingelegt?");
          displayout();
          Serial.println("SD Card: appending to file failed.");
          delay(5000);
      } else {
          sprintf(displaybuffer, "schreibe Daten\nauf SD-Karte ...");
          displayout();
          Serial.printf("SD Card: appending data to %s.\n", SDCARD_FILE);
          sdwrite.println(String(sdcardbuffer));
          sdwrite.close();
          delay(5000);
      }
}
//*************************************************************************


//*************************************************************************
void midnight_or_reboot(){
  if (datum != letztesdatum) {                              // datum = 1-31, letztesdatum nach Initialisierung=0
    gewicht_mitternacht = aktuellesgewicht;
    letztesgewicht = aktuellesgewicht;
    gewichtskorrektur = 0;
    if (verboseSerial) {
      Serial.print(F("Referenzgewicht Beute LBST"));
      Serial.print(F(" um Mitternacht oder nach reboot = "));
      Serial.println(gewicht_mitternacht);
    }
  }
}
//*************************************************************************


//*************************************************************************
String getWeight(Gewicht _gewicht, HX711 _scale){
  _scale.set_offset(_gewicht.tara);
  _scale.set_scale(_gewicht.scale);
  // j=Anzahl der Wiederholungen, wenn Abweichung zum letzten Gewicht zu hoch
  for(byte j=0 ;j < 4; j++) {
    //_gewicht.weight = _scale.get_units(10);                   // 10 samples
    _gewicht.weight = _scale.get_units(1);                     // nur 1 sample wegen Schwankungen -> besser?
    long temp_weight = _gewicht.weight - _gewicht.lastweight;
    if ((temp_weight < 200) and (temp_weight > -200)) j = 10;  // Abweichung für Fehlererkennung
    if ((_gewicht.weight > 200000 ) or (_gewicht.weight < 0)) _gewicht.weight = _gewicht.lastweight;    // unsinnige Werte verwerfen
    if (j < 4) delay(100);                                    // Wartezeit zwischen Wiederholungen
  }

  // Temperaturkompensation noch einfügen -> siehe Webseite beelogger

 /*
  if ((TempOut != 999.99)){
    if (TempOut > Kalibriertemperatur) Gewicht = Gewicht-(fabs(TempOut-Kalibriertemperatur)*KorrekturwertGrammproGrad);
    if (TempOut < Kalibriertemperatur) Gewicht = Gewicht+(fabs(TempOut-Kalibriertemperatur)*KorrekturwertGrammproGrad);
  }
 */

  _gewicht.lastweight = _gewicht.weight;
  if (verboseSerial){
    Serial.print("HX" + String(_gewicht.id) + "\t");
    Serial.print(_gewicht.weight);
    Serial.print(" g\r");
  }
  yield();
  return String(_gewicht.weight);
}
//*************************************************************************


//*************************************************************************
void blink(){
  digitalWrite(LED_BUILTIN, 1);
  delay(50);
  digitalWrite(LED_BUILTIN, 0);
  delay(50);
  digitalWrite(LED_BUILTIN, 1);
  delay(50);
  digitalWrite(LED_BUILTIN, 0);
}
//*************************************************************************


//*************************************************************************
void setup() {
  Serial.begin(115200);
  delay (100);
  delay(5000);

  if (verboseSerial) {
    Serial.println("*******************************************************************************");
    Serial.println("              Messung Temperatur und Gewicht");
    Serial.println("            (absolut/relativ gegen Wert um Mitternacht");
    Serial.println("         einer Beute am LBST ohne upload irgendwohin \r\n");
    Serial.print("              Messungen alle und erste Messung in -> ");
    Serial.print(update_interval/60000);
    Serial.println(" <- Minuten.\r\n");
    Serial.println("     Setup: Initialisiere ...");
    Serial.println("*******************************************************************************");
  }

  // init rtc, sd, display, LED
  pinMode(LED_BUILTIN, OUTPUT);

  blink();
  Wire.begin(I2Csda, I2Cscl);                   // for RTC DS3231
  initDisplay();                                // initialisiere OLED display
  sprintf(displaybuffer, "LBST\nDigiBEES");
  display.setFont(ArialMT_Plain_24);            // Textgröße 10, 16 oder 24
  displayout();
  delay(5000);

  initSD();                                     // initialisiere SD-Karte
  
  delay(5000);

  sprintf(displaybuffer, "TTGO esp32\nLoRa V2.1");
  displayout();
  delay(5000);

  sprintf(displaybuffer, "Kontakt:\nditmar@d-unger.de");
  display.setFont(ArialMT_Plain_10);          
  displayout();
  delay(5000);

  // initialisiere HX711
  scale[0].begin(gewicht[0].data, gewicht[0].clock);
  delay(1000);
}
//*************************************************************************


//*************************************************************************
void loop() {
  rtc.refresh();
  display.setFont(ArialMT_Plain_24);          // 10, 16 oder 24
  
  // 10x/10 Sek. Datum + Uhrzeit
  blink();
  for (int j = 0; j < 10; j++) {
    sprintf(displaybuffer, "LBST %d.%d\n%02d:%02d:%02d", rtc.day(), rtc.month(), rtc.hour(), rtc.minute(), rtc.second());
    rtc.refresh();
    displayout();
    delay(1000);
  }

  // 10x/10 Sek. Temperatur
  blink();
  for (int j = 0; j < 10; j++) {
    sprintf(displaybuffer, "Temp =\n%5.1f °C", rtc.temp()/100.);
    rtc.refresh();
    displayout();
    delay(1000);
  }

  // 10x/10 Sek. Gewicht
  blink();
  for (int j = 0; j < 10; j++) {
    strgewicht = getWeight(gewicht[0], scale[0]);
    aktuellesgewicht = strgewicht.toInt(); 
    if (verboseSerial) Serial.println("Gewicht = " + strgewicht + "g");
    sprintf(displaybuffer, "Gewicht =\n%d g", aktuellesgewicht );
    displayout();
    delay(1000);
  }

  // Gewichtsdifferenz der letzten zur vorletzten 5-Minuten Messung
  blink();
  if (verboseSerial) Serial.println("Gewichtsdifferenz = " + String(gewichtsdifferenz) + "g");
  sprintf(displaybuffer, "G_differenz\n%d g", gewichtsdifferenz );
  displayout();
  delay(10000);

  // Gewichtskorrektur der letzten zur vorletzten 5-Minuten Messung
  blink();
  if (verboseSerial) Serial.println("Gewichtskorrektur = " + String(gewichtskorrektur) + "g");
  sprintf(displaybuffer, "G_korrektur\n%d g", gewichtskorrektur );
  displayout();
  delay(10000);

  // Gewichtszunahme der letzten 5-Minuten Messung zum Wert an Mitternacht bzw. nach reboot
  blink();
  if (verboseSerial) Serial.println("Gewichtszunahme = " + String(gewichtszunahmeprotag) + "g");
  sprintf(displaybuffer, "G_zunahme\n%d g", gewichtszunahmeprotag );
  displayout();
  delay(10000);

  // Start Messungen wenn update_interval überschritten (alle 5 Minuten):
  if (millis() - update_interval_tmp > update_interval) {

    // hole Tag des Monats (01-31) von RTC
    datum = rtc.day();
    rtc.refresh();
    if (verboseSerial) Serial.println("Tag = " + String(datum));
    
    // Abfrage Gewicht absolut und Gewichtsveränderung gegen Wert Mitternacht der Beute:
    strgewicht = getWeight(gewicht[0], scale[0]);
    aktuellesgewicht = strgewicht.toInt();
    if (verboseSerial) Serial.println("Gewicht = " + strgewicht + "g");

    // ungefähr um Mitternacht und nach reboot Datumswechsel erkennen, Bienenzähler auf Null setzen und Referenzgewicht setzen:
    midnight_or_reboot();

    // Änderungen im Gewicht > +/-200g innerhalb von 5 Min. kommen vom Imker und nicht von den Bienen -> Korrektur
    gewichtsdifferenz = aktuellesgewicht - letztesgewicht;
    if ( abs(gewichtsdifferenz) > 200 ) {
        gewichtskorrektur += gewichtsdifferenz;            // kann mehrmals pro Tag vorkommen -> addiere auf pro Tag
    }
    gewichtszunahmeprotag = aktuellesgewicht - gewicht_mitternacht - gewichtskorrektur;

    // Umspeichern:
    letztesgewicht = aktuellesgewicht;
    letztesdatum = datum;
    update_interval_tmp = millis();

    sprintf(sdcardbuffer, "20%02d-%02d-%02d %02d:%02d:%02d;%5.1f;%d;%d;%d",
            rtc.year(), rtc.month(), rtc.day(), rtc.hour(), rtc.minute(), rtc.second(), rtc.temp()/100., aktuellesgewicht, gewichtskorrektur, gewichtszunahmeprotag );
    Serial.println("sdcardbuffer = " + String(sdcardbuffer));
    if (verboseSerial) Serial.println("Schreibe Messungen auf sd-Karte ...");
    blink();
    sdout();

    delay(100);
  }
}
//*************************************************************************

// ENDE
